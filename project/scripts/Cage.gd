extends Area2D

export var gem_threshold := 1

var _payable_node = null
var _faery_res = load("res://scenes/Faery.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	# Create a Faery
	var _faery = _faery_res.instance()
	var vitality = 0.50 / gem_threshold
	_faery.name = "CagedFaery"
	_faery.position.x = -25
	_faery.position.y = 168
	_faery.caged = true
	_faery.get_node("Caged/PathFollow2D").modulate = Color(vitality, vitality, vitality)
	add_child(_faery)
	connect("area_entered", self, "free_faery")
	connect("area_exited", self, "ignore_faery")

func free_faery(object):
	if object.name == "PlayerArea":
		_payable_node = object.get_parent()
		_payable_node.cage_contact = self
		
func pay():
	var faery
	var num = 0
	if GameMan.inventory.gems >= gem_threshold:
		GameMan.inventory.gems -= gem_threshold
		_payable_node.sparkle_faeries()
		_payable_node.cage_contact = null
		faery = get_node("CagedFaery")
		self.remove_child(faery)
		num = _payable_node.get_node("Faeries").get_child_count()
		faery = _payable_node.add_faery(num)
		faery.value *= gem_threshold
		queue_free()
		
func ignore_faery(object):
	if object.name == "PlayerArea":
		if is_instance_valid(_payable_node) && !_payable_node.is_queued_for_deletion():
			_payable_node.cage_contact = null
