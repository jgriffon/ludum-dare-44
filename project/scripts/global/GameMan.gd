extends Node

var inventory = {
		"gems": 0,
		"life" : 10
	}

var account = {
		"interest": 0.00,
		"accrued": 0.00,
		"time": 0,
		"frequency": 365
	}

var player_instance

func compound():
	var rate = account.interest / account.frequency
	var growth = pow((1 + rate), account.frequency * account.time)
	account.accrued *= growth
	print(account)

func cash_out():
	var bonus = round(account.accrued)
	inventory.gems += bonus
	account.accrued = 0.00
	account.time = 0
	account.interest = 0.00

func new_game():
	var intro_level = load("res://scenes/levels/IntroLevel.tscn")
	get_tree().change_scene_to(intro_level)
	