extends Node2D

var magnify =  3
var _faery

export var caged := false
export var value := 5
# Called when the node enters the scene tree for the first time.
func _ready():
	_faery = find_node("FaeryBody")
	_resparkle()
	
	if caged == true:
		enslave()

func _resparkle():
	_faery.get_node("Sparkle").amount = GameMan.inventory.gems * magnify
	#_faery.get_node("Sparkle").scale_amount = 2 / gems
	
func liberate():
	get_node("Caged").is_active = false
	get_node("Caged/PathFollow2D").remove_child(_faery)
	get_node("Free/PathFollow2D").add_child(_faery)
	get_node("Free").is_active = true
	
func enslave():
	get_node("Free").is_active = false
	get_node("Free/PathFollow2D").remove_child(_faery)
	get_node("Caged/PathFollow2D").add_child(_faery)
	get_node("Caged").is_active = true