extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var mound_trigger := true
var opened_trigger = true
var cage_trigger = true
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_MoundTrigger_area_entered(area):
	if mound_trigger == true:
		$Cutscene/AnimationPlayer.play("Mound")
		mound_trigger = false


func _on_Mound_mound_opened():
	if opened_trigger == true:
		$Cutscene/AnimationPlayer.play("MoundOpened")
		opened_trigger = false


func _on_CageTrigger_area_entered(area):
	if cage_trigger == true:
		$Cutscene/AnimationPlayer.play("Cage")
		cage_trigger = false
