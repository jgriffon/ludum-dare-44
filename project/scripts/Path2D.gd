extends Path2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var speed := 3.0
export var is_active = false
# Called when the node enters the scene tree for the first time.
func _process(delta):
	if is_active == true:
		get_node("PathFollow2D").offset += speed
#func _process(delta):
#	pass
