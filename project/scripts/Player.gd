extends KinematicBody2D

const UP = Vector2(0, -1)
const GRAVITY = 200
export var speed = 250
export var accel = 30
export var survivor_rate := 0.25
export var faeries := 0
var velocity = Vector2()
var snap = Vector2(0, 1)

var is_crouching = false
var mound_contact
var cage_contact
var gem_credit = 0

var _faery_res = load("res://scenes/Faery.tscn")

func _ready():
	randomize()
	# Load any faeries we may already have
	for i in faeries:
		add_faery(i)
	
func _physics_process(delta):
	velocity.y =  GRAVITY
	if Input.is_action_pressed("ui_right"):
		if is_crouching:
			is_crouching = false
			
		velocity.x = min(velocity.x + accel, speed)
		$Sprite.flip_h = false
		flip_faeries(false)
		$Sprite.play("walk")
	elif Input.is_action_pressed("ui_left"):
		if is_crouching:
			is_crouching = false
			
		velocity.x = max(velocity.x - accel, -speed)
		$Sprite.flip_h = true
		flip_faeries(true)
		$Sprite.play("walk")
	elif Input.is_action_just_pressed("ui_down"):
		is_crouching = true
		$Sprite.play("crouch")	
		if is_instance_valid(mound_contact):
			mound_contact.open()
		elif is_instance_valid(cage_contact):
			save_faery()
	elif Input.is_action_just_pressed("ui_up"):
		if is_instance_valid(cage_contact):
			sacrifice_faery()
	else:
		velocity.x = 0
		if (!is_crouching):
			$Sprite.play("default")	

	move_and_slide_with_snap(velocity, snap, UP, true)

func add_gem():
	GameMan.inventory.gems += 1
	sparkle_faeries()
	
func sacrifice_faery():
	var faery = $Faeries.get_child(0)

	if GameMan.account.accrued > 0.00:
		GameMan.cash_out()
	else:
		GameMan.inventory.gems += faery.value
	cage_contact.pay()
	$Faeries.remove_child(faery)
	faery.queue_free()
	sparkle_faeries()
	cage_contact = null

func save_faery():
	var faeries = $Faeries.get_children()
	cage_contact.pay()
	
	GameMan.account.interest += survivor_rate
	GameMan.account.time += 1
	
	if faeries.size() > 0:
		if is_instance_valid(faeries[-1]):
			GameMan.account.accrued += faeries[-1].value
	
	GameMan.compound()

func flip_faeries(flip):
	var faeries = $Faeries.get_children()
	for faery in faeries:
		faery.get_node("Free/PathFollow2D/FaeryBody/AnimatedSprite").flip_h = flip
		
func add_faery(i):
	var faery = _faery_res.instance()
	faery.name = "Faery" + str(i) 
	faery.position.x = 0 - (i * 20)
	faery.position.y = 0 + (i * 10)
	faery.get_node("Free/PathFollow2D").offset = randi() % 600
	get_node("Faeries").add_child(faery)
	
	return faery

func sparkle_faeries():
	var faeries = $Faeries.get_children()
	for faery in faeries:
		faery._resparkle()