extends StaticBody2D

signal mound_opened

var is_open = false
var texture_open = preload("res://assets/sprites/mound2.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("Area2D").connect("area_entered", self, "search")

func search(object):
	if object.name == "PlayerArea":
		object.get_parent().mound_contact = self
		
func open():
	if is_open == true:
		get_node("Sprite").hide()
		get_node("Collision").disabled = true
		get_node('../Player').add_gem()
		get_node("Gem/Animation").play("collect")
		get_node('../Player').mound_contact = null
		yield(get_node("Gem/Animation"), "animation_finished")
		queue_free()
	else:
		emit_signal("mound_opened")
		get_node("Sprite").texture = texture_open
		is_open = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
